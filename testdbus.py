#!/usr/bin/env python3
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject

def sr(interface,changed,invalidated,path):
    print(interface,changed,path)

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
bus = dbus.SystemBus()
bus.add_signal_receiver(sr, signal_name="PropertiesChanged", dbus_interface="org.freedesktop.DBus.Properties", path_keyword="path")

mainloop = gobject.MainLoop()
mainloop.run()
