#!/usr/bin/env python3

# blueagent5.py
# Dependencies: python-gobject (install with e.g. 'sudo apt-get install python-gobject' on Raspian
# Author: Douglas Otwell
# This software is released to the public domain

# The Software is provided "as is" without warranty of any kind, either express or implied, 
# including without limitation any implied warranties of condition, uninterrupted use, 
# merchantability, fitness for a particular purpose, or non-infringement.

import time
import sys
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject
import logging
from optparse import OptionParser
import subprocess

SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'

#LOG_LEVEL = logging.INFO
LOG_FILE = "/home/echo/dev/echo/log"
LOG_LEVEL = logging.DEBUG
#LOG_FILE = sys.stdout
LOG_FORMAT = "%(asctime)s %(levelname)s [%(module)s] %(message)s"

"""GPL LICENSED CODE FROM BLUEZ5"""

def findAdapter(bus):
    manager = dbus.Interface(bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
    objects = manager.GetManagedObjects()
    print(objects)
    for path, ifaces in objects.items():
        adapter = ifaces.get("org.bluez.Adapter1")
        if adapter is None:
            continue
        obj = bus.get_object("org.bluez", path)
        return dbus.Interface(obj, "org.bluez.Adapter1")
    raise Exception("Bluetooth adapter not found")

"""END GPL LICENSED CODE"""


class BlueAgent(dbus.service.Object):
    bus = None
    AGENT_PATH = "/blueagent5/agent"
    CAPABILITY = "NoInputNoOutput"
    pin_code = None

    def __init__(self, pin_code):
        dbus.service.Object.__init__(self, dbus.SystemBus(), BlueAgent.AGENT_PATH)
        self.bus = dbus.SystemBus()
        self.pin_code = pin_code

        logging.basicConfig(filename=LOG_FILE, format=LOG_FORMAT, level=LOG_LEVEL)
        #logging.basicConfig(stream=LOG_FILE, format=LOG_FORMAT, level=LOG_LEVEL)
        logging.info("Starting BlueAgent with PIN [{}]".format(self.pin_code))
        self.bus.add_signal_receiver(self.sr_factory(), dbus_interface="org.freedesktop.DBus.Properties", signal_name="PropertiesChanged", path_keyword="path")

    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def DisplayPinCode(self, device, pincode):
        logging.debug("BlueAgent DisplayPinCode invoked")

    @dbus.service.method(AGENT_IFACE, in_signature="ouq", out_signature="")
    def DisplayPasskey(self, device, passkey, entered):
        logging.debug("BlueAgent DisplayPasskey invoked")

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="s")
    def RequestPinCode(self, device):
        logging.info("BlueAgent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return self.pin_code

    @dbus.service.method(AGENT_IFACE, in_signature="ou", out_signature="")
    def RequestConfirmation(self, device, passkey):
        """Always confirm"""
        logging.info("BlueAgent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return

    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def AuthorizeService(self, device, uuid):
        """Always authorize"""
        logging.debug("BlueAgent AuthorizeService method invoked")
        return

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="u")
    def RequestPasskey(self, device):
        logging.debug("RequestPasskey returns 0")
        return dbus.UInt32(0)

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="")
    def RequestAuthorization(self, device):
        """Always authorize"""
        logging.info("BlueAgent is authorizing device [{}]".format(self.device))
        return

    @dbus.service.method(AGENT_IFACE, in_signature="", out_signature="")
    def Cancel(self):
        logging.info("BlueAgent pairing request canceled from device [{}]".format(self.device))

    def trustDevice(self, path):
        device_properties = dbus.Interface(self.bus.get_object(SERVICE_NAME, path), "org.freedesktop.DBus.Properties")
        device_properties.Set(DEVICE_IFACE, "Trusted", True)

    def registerAsDefault(self):
        manager = dbus.Interface(self.bus.get_object(SERVICE_NAME, "/org/bluez"), "org.bluez.AgentManager1")
        manager.RegisterAgent(BlueAgent.AGENT_PATH, BlueAgent.CAPABILITY)
        manager.RequestDefaultAgent(BlueAgent.AGENT_PATH)

    def startPairing(self):
        adapterProps = self.getAdapterProps()
        dev_address = adapterProps.Get(ADAPTER_IFACE, "Address")
        logging.debug(dev_address)
        adapterProps.Set(ADAPTER_IFACE, "Powered", True)
        # adapter.Disconnect() # TODO: auto-reconnect with device later
        # self.handleConnected(False) # pretend we just got disconnected
        
        logging.info("BlueAgent is waiting to pair with device")

    def rfKillInternalWiFi(self, action):
        logging.debug("rfkill wifi: " + str(action))
        rfkill = dbus.Interface(self.bus.get_object("org.freedesktop.URfkill", "/org/freedesktop/URfkill"), "org.freedesktop.URfkill")
        devices = rfkill.EnumerateDevices()
        for _, path in enumerate(devices):
            rfkill_properties_interface = dbus.Interface(self.bus.get_object("org.freedesktop.URfkill", path), "org.freedesktop.DBus.Properties")
            rfkill_iface_type = rfkill_properties_interface.Get("org.freedesktop.URfkill.Device","type")
            if rfkill_iface_type == 1: #TODO: only block internal RasPi WiFi.
                rfkill.BlockIdx(rfkill_properties_interface.Get("org.freedesktop.URfkill.Device","index"), action)

    def getAdapterProps(self):
        adapter_path = findAdapter(self.bus).object_path
        return dbus.Interface(self.bus.get_object(SERVICE_NAME, adapter_path), "org.freedesktop.DBus.Properties")

    def setDiscoverable(self, action):
        adapter = self.getActualAdapter()
        adapter.Set(ADAPTER_IFACE, "Discoverable", bool(action))
        
    def speak(self, phrase):
        process = subprocess.Popen(['/usr/bin/festival', '--tts'], stdin=subprocess.PIPE)
        process.communicate(bytes(phrase, 'utf-8')) # bytes is important

    def handleConnected(self, connected):
        print("called handleConnected")
        if connected:
            self.speak("device connected")
        elif not connected:
            self.speak("device disconnected")
        self.rfKillInternalWiFi(connected)
        self.setDiscoverable(not connected)

    def sr_factory(self):
        def signal_receiver(interface,changed,invalidated,path):
            if (interface == "org.bluez.Device1"):
                logging.debug(interface)
                logging.debug(path)
                logging.debug(changed)
                if(changed.get("Connected") is not None):
                    self.handleConnected(bool(changed.get("Connected")))
        return signal_receiver

if __name__ == "__main__":
    pin_code = "0000"
    parser = OptionParser()
    parser.add_option("-p", "--pin", action="store", dest="pin_code", help="PIN code to pair with", metavar="PIN")
    (options, args) = parser.parse_args()

    # use the pin code if provided
    if (options.pin_code):
        pin_code = options.pin_code

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    agent = BlueAgent(pin_code)
    agent.registerAsDefault()
    agent.startPairing()

    mainloop = gobject.MainLoop()
    mainloop.run()

